﻿#define DEMO

using GDGame.Controllers;
using GDGame.MyGame.Managers;
using GDLibrary;
using GDLibrary.Actors;
using GDLibrary.Constants;
using GDLibrary.Containers;
using GDLibrary.Controllers;
using GDLibrary.Core.Controllers;
using GDLibrary.Core.Managers.State;
using GDLibrary.Debug;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.Factories;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.Parameters;
using GDLibrary.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace GDGame
{
    public class Main : Game
    {
        #region Fields

        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private CameraManager<Camera3D> cameraManager;
        private ObjectManager objectManager;
        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        private RenderManager renderManager;
        private UIManager uiManager;
        private MyMenuManager menuManager;
        private SoundManager soundManager;
        private MyGameStateManager gameStateManager;

        //used to process and deliver events received from publishers
        private EventDispatcher eventDispatcher;

        //store useful game resources (e.g. effects, models, rails and curves)
        private Dictionary<string, BasicEffect> effectDictionary;

        //use ContentDictionary to store assets (i.e. file content) that need the Content.Load() method to be called
        private ContentDictionary<Texture2D> textureDictionary;

        private ContentDictionary<SpriteFont> fontDictionary;
        private ContentDictionary<Model> modelDictionary;

        //use normal Dictionary to store objects that do NOT need the Content.Load() method to be called (i.e. the object is not based on an asset file)
        private Dictionary<string, Transform3DCurve> transform3DCurveDictionary;

        //stores the rails used by the camera
        private Dictionary<string, RailParameters> railDictionary;

        //stores the archetypal primitive objects (used in Main and LevelLoader)
        private Dictionary<string, PrimitiveObject> archetypeDictionary;

        //defines centre point for the mouse i.e. (w/2, h/2)
        private Vector2 screenCentre;

        #endregion Fields

        #region Constructors

        public Main()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        #endregion Constructors

        #region Debug
#if DEBUG

        private void InitDebug()
        {
            InitDebugInfo(true);
            bool bShowCDCRSurfaces = true;
            bool bShowZones = true;
            InitializeDebugCollisionSkinInfo(bShowCDCRSurfaces, bShowZones, Color.White);
        }

        private void InitializeDebugCollisionSkinInfo(bool bShowCDCRSurfaces, bool bShowZones, Color boundingBoxColor)
        {
            //draws CDCR surfaces for boxes and spheres
            PrimitiveDebugDrawer primitiveDebugDrawer =
                new PrimitiveDebugDrawer(this, StatusType.Drawn | StatusType.Update,
                objectManager, cameraManager,
                bShowCDCRSurfaces, bShowZones);

            primitiveDebugDrawer.DrawOrder = 5;
            BoundingBoxDrawer.BoundingBoxColor = boundingBoxColor;

            Components.Add(primitiveDebugDrawer);
        }

        private void InitDebugInfo(bool bEnable)
        {
            if (bEnable)
            {
                //create the debug drawer to draw debug info
                DebugDrawer debugInfoDrawer = new DebugDrawer(this, _spriteBatch,
                    Content.Load<SpriteFont>("Assets/Fonts/debug"),
                    cameraManager, objectManager);

                //set the debug drawer to be drawn AFTER the object manager to the screen
                debugInfoDrawer.DrawOrder = 2;

                //add the debug drawer to the component list so that it will have its Update/Draw methods called each cycle.
                Components.Add(debugInfoDrawer);
            }
        }

#endif
        #endregion Debug

        #region Load - Assets

        private void LoadSounds()
        {
            //soundManager.Add(new GDLibrary.Managers.Cue("smokealarm",
            //    Content.Load<SoundEffect>("Assets/Audio/Effects/smokealarm1"), SoundCategoryType.Alarm, new Vector3(1, 0, 0), false));

            //to do..add more sounds
        }

        private void LoadEffects()
        {
            //to do...
            BasicEffect effect = null;

            //used for unlit primitives with a texture (e.g. textured quad of skybox)
            effect = new BasicEffect(_graphics.GraphicsDevice);
            effect.VertexColorEnabled = true; //otherwise we wont see RGB
            effect.TextureEnabled = true;
            effectDictionary.Add(GameConstants.Effect_UnlitTextured, effect);

            effect = new BasicEffect(_graphics.GraphicsDevice);
            effect.TextureEnabled = true;
            effectDictionary.Add(GameConstants.Effect_UnlitTexturedPrimitive, effect);

            //used for wireframe primitives with no lighting and no texture (e.g. origin helper)
            effect = new BasicEffect(_graphics.GraphicsDevice);
            effect.VertexColorEnabled = true;
            effectDictionary.Add(GameConstants.Effect_UnlitWireframe, effect);

            //to do...add a new effect to draw a lit textured surface (e.g. a lit pyramid)
            effect = new BasicEffect(_graphics.GraphicsDevice);
            effect.TextureEnabled = true;
            effect.LightingEnabled = true; //redundant?
            effect.PreferPerPixelLighting = true; //cost GPU cycles
            effect.EnableDefaultLighting();
            //change lighting position, direction and color

            effectDictionary.Add("lit textured", effect);
        }

        private void LoadTextures()
        {
            //level 1 where each image 1_1, 1_2 is a different Y-axis height specificied when we use the level loader
            textureDictionary.Load("Assets/Textures/Level/level1_1");
            textureDictionary.Load("Assets/Textures/Level/level1_2");
            textureDictionary.Load("Assets/Textures/Level/level1_3");
            textureDictionary.Load("Assets/Textures/Level/level2_1");         
            textureDictionary.Load("Assets/Textures/Level/level2_2");         
            textureDictionary.Load("Assets/Textures/Level/level2_3");         

            //sky
            textureDictionary.Load("Assets/Textures/Skybox/back");
            textureDictionary.Load("Assets/Textures/Skybox/left");
            textureDictionary.Load("Assets/Textures/Skybox/right");
            textureDictionary.Load("Assets/Textures/Skybox/front");
            textureDictionary.Load("Assets/Textures/Skybox/top");

            //architecture
            textureDictionary.Load("Assets/Textures/Architecture/Floor/34636");
            textureDictionary.Load("Assets/Textures/Architecture/Floor/34690");
            textureDictionary.Load("Assets/Textures/Architecture/Walls/brick");

            //demo
            textureDictionary.Load("Assets/Demo/Textures/checkerboard");

            //ui
            textureDictionary.Load("Assets/Textures/UI/Controls/progress_white");
            textureDictionary.Load("Assets/Textures/UI/Controls/progress_bar");

            //shapes
            textureDictionary.Load("Assets/Textures/Shapes/octahedron");
            textureDictionary.Load("Assets/Textures/Shapes/hero block");
            textureDictionary.Load("Assets/Textures/Shapes/mystery block");

            //menu
            textureDictionary.Load("Assets/Textures/UI/Controls/button");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/Menu background 1080p");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/how to play background");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/select level background");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/win background");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/lose background");

            //ui
            textureDictionary.Load("Assets/Textures/UI/Controls/reticuleDefault");

            //add more...
        }

        private void LoadFonts()
        {
            fontDictionary.Load("Assets/Fonts/debug");
            fontDictionary.Load("Assets/Fonts/menu");
            fontDictionary.Load("Assets/Fonts/ui");
        }

        #endregion Load - Assets

        #region Initialization - Graphics, Managers, Dictionaries, Cameras, Menu, UI

        protected override void Initialize()
        {
            //set game title
            Window.Title = "Super Ultra Mega Cube 3D";

            //graphic settings - see https://en.wikipedia.org/wiki/Display_resolution#/media/File:Vector_Video_Standards8.svg
            InitGraphics(1024, 768);

            //note that we moved this from LoadContent to allow InitDebug to be called in Initialize
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            //create event dispatcher
            InitEventDispatcher();

            //dictionaries
            InitDictionaries();

            //curves and rails used by cameras
            InitCurves();
            InitRails();

            //load from file or initialize assets, effects and vertices
            LoadEffects();
            LoadTextures();
            LoadFonts();
            LoadSounds();

            //add archetypes that can be cloned
            InitArchetypes();

            //NOTE: drawn content and cameras are initialised in MyGameStateManager.cs for easier handling of drawn content when restarting
            //InitLevel(worldScale);

            //InitCameras3D();

            //managers
            InitManagers();

            //audio
            InitAudio();
            InitGameMusic();

            //ui
            InitUI();
            InitMenu();

            #region Debug
#if DEBUG
            //debug info
            //InitDebug();
#endif
            #endregion Debug

            base.Initialize();
        }

        private void InitGraphics(int width, int height)
        {
            //set resolution
            _graphics.PreferredBackBufferWidth = width;
            _graphics.PreferredBackBufferHeight = height;

            //dont forget to apply resolution changes otherwise we wont see the new WxH
            _graphics.ApplyChanges();

            //set screen centre based on resolution
            screenCentre = new Vector2(width / 2, height / 2);

            //set cull mode to show front and back faces - inefficient but we will change later
            RasterizerState rs = new RasterizerState();
            rs.CullMode = CullMode.None;
            _graphics.GraphicsDevice.RasterizerState = rs;

            //we use a sampler state to set the texture address mode to solve the aliasing problem between skybox planes
            SamplerState samplerState = new SamplerState();
            samplerState.AddressU = TextureAddressMode.Clamp;
            samplerState.AddressV = TextureAddressMode.Clamp;
            _graphics.GraphicsDevice.SamplerStates[0] = samplerState;

            //set blending
            _graphics.GraphicsDevice.BlendState = BlendState.AlphaBlend;

            //set screen centre for use when centering mouse
            screenCentre = new Vector2(width / 2, height / 2);
        }

        private void InitUI()
        {
            Transform2D transform2D = null;
            Texture2D texture = null;
            SpriteFont spriteFont = null;

            //progress bar for pickups
            #region Progress Control Left
            texture = textureDictionary["progress_bar"];

            transform2D = new Transform2D(new Vector2(490, 40),
                0,
                 new Vector2(1.5f, 1.5f),
                new Vector2(texture.Width / 2, texture.Height / 2),
                new Integer2(100, 100));

            UITextureObject uiTextureObject = new UITextureObject("progress 1", ActorType.UITextureObject,
                StatusType.Drawn | StatusType.Update, transform2D, Color.Yellow, 0, SpriteEffects.None,
                texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));

            uiTextureObject.ControllerList.Add(new UIProgressController("pc1", ControllerType.Progress, 0, 6));

            uiManager.Add(uiTextureObject);
            #endregion Progress Control Left
        }

        private void InitMenu()
        {
            Texture2D texture = null;
            Transform2D transform2D = null;
            DrawnActor2D uiObject = null;
            Vector2 fullScreenScaleFactor = Vector2.One;

            #region All Menu Background Images
            //background main
            texture = textureDictionary["Menu background 1080p"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);

            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("main_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("main", uiObject);

            //background controls
            texture = textureDictionary["how to play background"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("howtoplay_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("howtoplay", uiObject);

            //background select level
            texture = textureDictionary["select level background"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("selectlevel_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("select level", uiObject);

            //background win
            texture = textureDictionary["win background"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("win_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("win", uiObject);

            //background lose
            texture = textureDictionary["lose background"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("lose_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("lose", uiObject);

            #endregion All Menu Background Images

            //main menu buttons
            texture = textureDictionary["button"];

            Vector2 origin = new Vector2(texture.Width / 2, texture.Height / 2);

            Integer2 imageDimensions = new Integer2(texture.Width, texture.Height);

            //play
            transform2D = new Transform2D(screenCentre - new Vector2(290, -250), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("play", ActorType.UITextureObject, StatusType.Update | StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "Insert coin",
                fontDictionary["menu"],
                new Vector2(1, 1),
                Color.Magenta,
                new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Red, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.02f, 1, 0)));

            menuManager.Add("main", uiObject);

            //how to play
            transform2D = new Transform2D(screenCentre + new Vector2(0, 250), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("howtoplay", ActorType.UITextureObject,
                StatusType.Update | StatusType.Drawn,
             transform2D, Color.White, 1, SpriteEffects.None, texture,
             new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
             "How to play",
             fontDictionary["menu"],
             new Vector2(1, 1),
             Color.Magenta,
             new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Red, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.02f, 1, 0)));

            menuManager.Add("main", uiObject);

            //exit
            transform2D = new Transform2D(screenCentre + new Vector2(290, 250), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("exit", ActorType.UITextureObject,
                StatusType.Update | StatusType.Drawn,
             transform2D, Color.White, 1, SpriteEffects.None, texture,
             new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
             "Exit",
             fontDictionary["menu"],
             new Vector2(1, 1),
             Color.Magenta,
             new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Red, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.02f, 1, 0)));

            menuManager.Add("main", uiObject);

            //how to play - back button
            transform2D = new Transform2D(screenCentre + new Vector2(0, 180), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("back", ActorType.UITextureObject,
                StatusType.Update | StatusType.Drawn,
             transform2D, Color.White, 1, SpriteEffects.None, texture,
             new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
             "Back",
             fontDictionary["menu"],
             new Vector2(1, 1),
             Color.Magenta,
             new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Red, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.02f, 1, 0)));

            menuManager.Add("howtoplay", uiObject);

            //win and lose menu
            transform2D = new Transform2D(screenCentre + new Vector2(0, 150), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("menu", ActorType.UITextureObject,
                StatusType.Update | StatusType.Drawn,
             transform2D, Color.White, 1, SpriteEffects.None, texture,
             new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
             "Menu",
             fontDictionary["menu"],
             new Vector2(1, 1),
             Color.Magenta,
             new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Red, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.02f, 1, 0)));

            menuManager.Add("win", uiObject);
            menuManager.Add("lose", uiObject);

            //level 1
            transform2D = new Transform2D(screenCentre + new Vector2(-200, 50), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("level 1", ActorType.UITextureObject,
                StatusType.Update | StatusType.Drawn,
             transform2D, Color.White, 1, SpriteEffects.None, texture,
             new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
             "Block maze",
             fontDictionary["menu"],
             new Vector2(1, 1),
             Color.Magenta,
             new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Red, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.02f, 1, 0)));

            menuManager.Add("select level", uiObject);

            //level 2
            transform2D = new Transform2D(screenCentre + new Vector2(200, 50), 0, new Vector2(1.7f, 1), origin, imageDimensions);
            uiObject = new UIButtonObject("level 2", ActorType.UITextureObject,
                StatusType.Update | StatusType.Drawn,
             transform2D, Color.White, 1, SpriteEffects.None, texture,
             new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
             "Dungeons and octahedrons",
             fontDictionary["menu"],
             new Vector2(1, 1),
             Color.Magenta,
             new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Red, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.02f, 1, 0)));

            menuManager.Add("select level", uiObject);

            //finally dont forget to SetScene to say which menu should be drawn/updated!
            menuManager.SetScene("main");
        }

        private void InitEventDispatcher()
        {
            eventDispatcher = new EventDispatcher(this);
            Components.Add(eventDispatcher);
        }

        private void InitCurves()
        {
            //path for enemy 1 to follow
            Transform3DCurve curveA = new Transform3DCurve(CurveLoopType.Cycle);
            curveA.Add(new Vector3(-35, 6, 390), -Vector3.UnitZ, Vector3.UnitY, 0); 
            curveA.Add(new Vector3(550, 6, 390), -Vector3.UnitZ, Vector3.UnitY, 5000);
            curveA.Add(new Vector3(550, 6, 390), -Vector3.UnitZ, Vector3.UnitY, 5300);
            curveA.Add(new Vector3(550, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 8000);
            curveA.Add(new Vector3(550, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 7300);
            curveA.Add(new Vector3(470, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 10000);
            curveA.Add(new Vector3(470, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 10300);
            curveA.Add(new Vector3(470, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 14000);
            curveA.Add(new Vector3(470, 6, 170), -Vector3.UnitZ, Vector3.UnitY, 16500);
            curveA.Add(new Vector3(470, 6, 170), -Vector3.UnitZ, Vector3.UnitY, 16800);
            curveA.Add(new Vector3(350, 6, 170), -Vector3.UnitZ, Vector3.UnitY, 18000);
            curveA.Add(new Vector3(350, 6, 170), -Vector3.UnitZ, Vector3.UnitY, 18300);
            curveA.Add(new Vector3(470, 6, 170), -Vector3.UnitZ, Vector3.UnitY, 19700);
            curveA.Add(new Vector3(470, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 21000);
            curveA.Add(new Vector3(470, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 21300);
            curveA.Add(new Vector3(250, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 24000);
            curveA.Add(new Vector3(250, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 24300);
            curveA.Add(new Vector3(250, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 27000);
            curveA.Add(new Vector3(250, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 27300);
            curveA.Add(new Vector3(250, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 30000);
            curveA.Add(new Vector3(250, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 30300);
            curveA.Add(new Vector3(30, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 32000);
            curveA.Add(new Vector3(30, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 32300);
            curveA.Add(new Vector3(30, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 34000);
            curveA.Add(new Vector3(30, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 34300);
            curveA.Add(new Vector3(30, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 35300);
            curveA.Add(new Vector3(30, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 35600);
            curveA.Add(new Vector3(-17, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 36300);
            curveA.Add(new Vector3(-17, 6, 110), -Vector3.UnitZ, Vector3.UnitY, 36600);
            curveA.Add(new Vector3(-17, 6, 60), -Vector3.UnitZ, Vector3.UnitY, 36950);
            curveA.Add(new Vector3(-17, 6, 60), -Vector3.UnitZ, Vector3.UnitY, 37200);
            curveA.Add(new Vector3(280, 6, 60), -Vector3.UnitZ, Vector3.UnitY, 39700);
            curveA.Add(new Vector3(280, 6, 60), -Vector3.UnitZ, Vector3.UnitY, 40000);
            curveA.Add(new Vector3(280, 6, -120), -Vector3.UnitZ, Vector3.UnitY, 42000);
            curveA.Add(new Vector3(280, 6, -120), -Vector3.UnitZ, Vector3.UnitY, 42300);
            curveA.Add(new Vector3(220, 6, -120), -Vector3.UnitZ, Vector3.UnitY, 42650);
            curveA.Add(new Vector3(220, 6, -120), -Vector3.UnitZ, Vector3.UnitY, 42950);
            curveA.Add(new Vector3(220, 6, 0), -Vector3.UnitZ, Vector3.UnitY, 44000);
            curveA.Add(new Vector3(220, 6, 0), -Vector3.UnitZ, Vector3.UnitY, 44300);
            curveA.Add(new Vector3(-17, 6, 0), -Vector3.UnitZ, Vector3.UnitY, 46700);
            curveA.Add(new Vector3(-17, 6, 0), -Vector3.UnitZ, Vector3.UnitY, 47000);
            curveA.Add(new Vector3(-17, 6, -60), -Vector3.UnitZ, Vector3.UnitY, 47350);
            curveA.Add(new Vector3(-17, 6, -60), -Vector3.UnitZ, Vector3.UnitY, 47650);
            curveA.Add(new Vector3(160, 6, -60), -Vector3.UnitZ, Vector3.UnitY, 48700);
            curveA.Add(new Vector3(160, 6, -60), -Vector3.UnitZ, Vector3.UnitY, 49000);
            curveA.Add(new Vector3(-17, 6, -60), -Vector3.UnitZ, Vector3.UnitY, 50500);
            curveA.Add(new Vector3(-17, 6, -60), -Vector3.UnitZ, Vector3.UnitY, 50800);
            curveA.Add(new Vector3(-35, 6, 390), -Vector3.UnitZ, Vector3.UnitY, 54600);

            //add to the dictionary
            transform3DCurveDictionary.Add("enemy 1 route", curveA);

            //path for enemy 2 to follow
            Transform3DCurve curveB = new Transform3DCurve(CurveLoopType.Cycle);
            curveB.Add(new Vector3(340, 6, -120), -Vector3.UnitZ, Vector3.UnitY, 0);
            curveB.Add(new Vector3(340, 6, 0), -Vector3.UnitZ, Vector3.UnitY, 01100);
            curveB.Add(new Vector3(340, 6, 0), -Vector3.UnitZ, Vector3.UnitY, 01400);
            curveB.Add(new Vector3(540, 6, 0), -Vector3.UnitZ, Vector3.UnitY, 02800);
            curveB.Add(new Vector3(540, 6, 0), -Vector3.UnitZ, Vector3.UnitY, 03100);
            curveB.Add(new Vector3(540, 6, -65), -Vector3.UnitZ, Vector3.UnitY, 03450);
            curveB.Add(new Vector3(540, 6, -65), -Vector3.UnitZ, Vector3.UnitY, 03750);
            curveB.Add(new Vector3(440, 6, -65), -Vector3.UnitZ, Vector3.UnitY, 04500);
            curveB.Add(new Vector3(440, 6, -65), -Vector3.UnitZ, Vector3.UnitY, 04800);
            curveB.Add(new Vector3(540, 6, -65), -Vector3.UnitZ, Vector3.UnitY, 05600);
            curveB.Add(new Vector3(540, 6, -65), -Vector3.UnitZ, Vector3.UnitY, 05900);
            curveB.Add(new Vector3(525, 6, 370), -Vector3.UnitZ, Vector3.UnitY, 11300);
            curveB.Add(new Vector3(525, 6, 370), -Vector3.UnitZ, Vector3.UnitY, 11600);
            curveB.Add(new Vector3(-5, 6, 370), -Vector3.UnitZ, Vector3.UnitY, 16300);
            curveB.Add(new Vector3(-5, 6, 370), -Vector3.UnitZ, Vector3.UnitY, 16600);
            curveB.Add(new Vector3(-5, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 19300);
            curveB.Add(new Vector3(-5, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 19600);
            curveB.Add(new Vector3(50, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 21000);
            curveB.Add(new Vector3(50, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 21300);
            curveB.Add(new Vector3(50, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 24000);
            curveB.Add(new Vector3(50, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 24300);
            curveB.Add(new Vector3(50, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 27300);
            curveB.Add(new Vector3(50, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 27600);
            curveB.Add(new Vector3(270, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 30600);
            curveB.Add(new Vector3(270, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 30900);
            curveB.Add(new Vector3(270, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 33000);
            curveB.Add(new Vector3(270, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 33300);
            curveB.Add(new Vector3(270, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 36300);
            curveB.Add(new Vector3(270, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 36900);
            curveB.Add(new Vector3(490, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 38300);
            curveB.Add(new Vector3(490, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 38600);
            curveB.Add(new Vector3(490, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 41000);
            curveB.Add(new Vector3(490, 6, 330), -Vector3.UnitZ, Vector3.UnitY, 41300);
            curveB.Add(new Vector3(490, 6, 190), -Vector3.UnitZ, Vector3.UnitY, 42800);
            curveB.Add(new Vector3(490, 6, 190), -Vector3.UnitZ, Vector3.UnitY, 43000);
            curveB.Add(new Vector3(350, 6, 190), -Vector3.UnitZ, Vector3.UnitY, 44200);
            curveB.Add(new Vector3(350, 6, 190), -Vector3.UnitZ, Vector3.UnitY, 44500);
            curveB.Add(new Vector3(490, 6, 190), -Vector3.UnitZ, Vector3.UnitY, 45700);
            curveB.Add(new Vector3(490, 6, 190), -Vector3.UnitZ, Vector3.UnitY, 46000);
            curveB.Add(new Vector3(490, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 46200);
            curveB.Add(new Vector3(490, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 46400);
            curveB.Add(new Vector3(525, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 46800);
            curveB.Add(new Vector3(525, 6, 130), -Vector3.UnitZ, Vector3.UnitY, 47000);
            curveB.Add(new Vector3(525, 6, 60), -Vector3.UnitZ, Vector3.UnitY, 47800);
            curveB.Add(new Vector3(525, 6, 60), -Vector3.UnitZ, Vector3.UnitY, 48100);
            curveB.Add(new Vector3(280, 6, 60), -Vector3.UnitZ, Vector3.UnitY, 50100);
            curveB.Add(new Vector3(280, 6, 60), -Vector3.UnitZ, Vector3.UnitY, 50400);
            curveB.Add(new Vector3(280, 6, -120), -Vector3.UnitZ, Vector3.UnitY, 52400);
            curveB.Add(new Vector3(340, 6, -120), -Vector3.UnitZ, Vector3.UnitY, 52750);

            //add to the dictionary
            transform3DCurveDictionary.Add("enemy 2 route", curveB);

            //path for intro camera to follow
            Transform3DCurve cameraCurve = new Transform3DCurve(CurveLoopType.Linear);
            cameraCurve.Add(new Vector3(250, 5, 410), -Vector3.UnitZ, Vector3.UnitY, 0);
            cameraCurve.Add(new Vector3(250, 20, 450), -Vector3.UnitZ, Vector3.UnitY, 1000);
            cameraCurve.Add(new Vector3(250, 30, 700), -Vector3.UnitZ, Vector3.UnitY, 2000);
            cameraCurve.Add(new Vector3(-200, 30, 300), Vector3.UnitX, Vector3.UnitY, 3000);
            cameraCurve.Add(new Vector3(200, 30, -400), Vector3.UnitZ, Vector3.UnitY, 4000);
            cameraCurve.Add(new Vector3(600, 30, -200), -Vector3.UnitX, Vector3.UnitY, 5000);
            cameraCurve.Add(new Vector3(800, 30, 0), -Vector3.UnitX, Vector3.UnitY, 5700);
            cameraCurve.Add(new Vector3(500, 30, 700), -Vector3.UnitZ, Vector3.UnitY, 7200);

            transform3DCurveDictionary.Add("camera curve", cameraCurve);
        }

        private void InitRails()
        {
            //create the track to be applied to the non-collidable track camera 1
            railDictionary.Add("rail1", new RailParameters("rail1 - parallel to z-axis", new Vector3(20, 10, 50), new Vector3(20, 10, -50)));
        }

        private void InitDictionaries()
        {
            //stores effects
            effectDictionary = new Dictionary<string, BasicEffect>();

            //stores textures, fonts & models
            modelDictionary = new ContentDictionary<Model>("models", Content);
            textureDictionary = new ContentDictionary<Texture2D>("textures", Content);
            fontDictionary = new ContentDictionary<SpriteFont>("fonts", Content);

            //curves - notice we use a basic Dictionary and not a ContentDictionary since curves and rails are NOT media content
            transform3DCurveDictionary = new Dictionary<string, Transform3DCurve>();

            //rails - store rails used by cameras
            railDictionary = new Dictionary<string, RailParameters>();

            //used to store archetypes for primitives in the game
            archetypeDictionary = new Dictionary<string, PrimitiveObject>();
        }

        private void InitManagers()
        {
            //physics and CD-CR (moved to top because MouseManager is dependent)
            //to do - replace with simplified CDCR

            //camera
            cameraManager = new CameraManager<Camera3D>(this, StatusType.Off);
            Components.Add(cameraManager);

            Transform3D transform3D = null;
            Camera3D camera3D = null;
            Viewport viewPort = new Viewport(0, 0, 1024, 768);

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.Zero);

            camera3D = new Camera3D(GameConstants.Camera_NonCollidableCurveMainArena,
              ActorType.Camera3D, StatusType.Update, transform3D,
                        ProjectionParameters.StandardDeepSixteenTen, viewPort);

            camera3D.ControllerList.Add(
                new Curve3DController(GameConstants.Controllers_NonCollidableCurveMainArena,
                ControllerType.Curve,
                        transform3DCurveDictionary["enemy 1 route"])); //use the curve dictionary to retrieve a transform3DCurve by id

            cameraManager.Add(camera3D);

            //keyboard
            keyboardManager = new KeyboardManager(this);
            Components.Add(keyboardManager);

            //mouse
            mouseManager = new MouseManager(this, true, screenCentre);
            Components.Add(mouseManager);

            //object
            objectManager = new ObjectManager(this, StatusType.Off, 6, 10);
            Components.Add(objectManager);

            //render
            renderManager = new RenderManager(this, StatusType.Drawn, ScreenLayoutType.Single,
                objectManager, cameraManager);
            Components.Add(renderManager);

            //add in-game ui
            uiManager = new UIManager(this, StatusType.Off, _spriteBatch, 10);
            uiManager.DrawOrder = 4;
            Components.Add(uiManager);

            //add menu
            menuManager = new MyMenuManager(this, StatusType.Update | StatusType.Drawn, _spriteBatch,
                mouseManager, keyboardManager);
            menuManager.DrawOrder = 5; //highest number of all drawable managers since we want it drawn on top!
            Components.Add(menuManager);

            //sound
            soundManager = new SoundManager(this, StatusType.Update);
            Components.Add(soundManager);

            //game states
            gameStateManager = new MyGameStateManager(this, StatusType.Update, menuManager, objectManager, textureDictionary, archetypeDictionary, transform3DCurveDictionary, cameraManager,
                keyboardManager, mouseManager, effectDictionary);
            Components.Add(gameStateManager);
        }

        private void InitAudio()
        {
            //music and SFX
            this.soundManager.Add(new GDLibrary.Managers.Cue("gamemusic", Content.Load<SoundEffect>("Assets/Audio/Music/8 bit funk"), SoundCategoryType.BackgroundMusic, new Vector3(0.15f, 0, 0), false));

            this.soundManager.Add(new GDLibrary.Managers.Cue("menumusic", Content.Load<SoundEffect>("Assets/Audio/Music/retro platforming"), SoundCategoryType.BackgroundMusic, new Vector3(0.1f, 0, 0), true));

            this.soundManager.Add(new GDLibrary.Managers.Cue("intro", Content.Load<SoundEffect>("Assets/Audio/Music/SUMC3D intro"), SoundCategoryType.Intro, new Vector3(0.3f, 0, 0), false));

            this.soundManager.Add(new GDLibrary.Managers.Cue("pickup", Content.Load<SoundEffect>("Assets/Audio/Effects/pickup sound"), SoundCategoryType.WinLose, new Vector3(0.15f, 0, 0), false));

            this.soundManager.Add(new GDLibrary.Managers.Cue("win game", Content.Load<SoundEffect>("Assets/Audio/Effects/win game"), SoundCategoryType.WinLose, new Vector3(0.2f, 0, 0), false));

            this.soundManager.Add(new GDLibrary.Managers.Cue("lose game", Content.Load<SoundEffect>("Assets/Audio/Effects/lose game"), SoundCategoryType.WinLose, new Vector3(0.1f, 0, 0), false));

            this.soundManager.Add(new GDLibrary.Managers.Cue("menu select", Content.Load<SoundEffect>("Assets/Audio/Effects/menu select"), SoundCategoryType.Button, new Vector3(0.2f, 0, 0), false));
        }

        /// <summary>
        /// Sends an event to play the intro music and sound effect upon starting the game
        /// </summary>
        private void InitGameMusic()
        {
            object[] parameters = { "intro" };
            EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

            object[] parameters2 = { "menumusic" };
            EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters2));
        }

        #endregion Initialization - Graphics, Managers, Dictionaries, Cameras, Menu, UI

        #region Initialization - Vertices, Archetypes, Helpers, Drawn Content(e.g. Skybox)

        /// <summary>
        /// Creates archetypes used in the game.
        ///
        /// What are the steps required to add a new primitive?
        ///    1. In the VertexFactory add a function to return Vertices[]
        ///    2. Add a new BasicEffect IFF this primitive cannot use existing effects(e.g.wireframe, unlit textured)
        ///    3. Add the new effect to effectDictionary
        ///    4. Create archetypal PrimitiveObject.
        ///    5. Add archetypal object to archetypeDictionary
        ///    6. Clone archetype, change its properties (transform, texture, color, alpha, ID) and add manually to the objectmanager or you can use LevelLoader.
        /// </summary>
        private void InitArchetypes() //formerly InitTexturedQuad
        {
            Transform3D transform3D = null;
            EffectParameters effectParameters = null;
            IVertexData vertexData = null;
            PrimitiveType primitiveType;
            int primitiveCount;

            #region Lit Textured Pyramid

            /*********** Transform, Vertices and VertexData ***********/
            //unlit pyramid
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero,
                 Vector3.One, Vector3.UnitZ, Vector3.UnitY);
            effectParameters = new EffectParameters(effectDictionary[GameConstants.Effect_LitTextured],
                textureDictionary["checkerboard"], Color.White, 1);

            VertexPositionNormalTexture[] vertices
                = VertexFactory.GetVerticesPositionNormalTexturedPyramid(out primitiveType,
                out primitiveCount);

            //analog of the Model class in G-CA (i.e. it holdes vertices and type, count)
            vertexData = new VertexData<VertexPositionNormalTexture>(vertices,
                primitiveType, primitiveCount);

            /*********** PrimitiveObject ***********/
            //now we use the "FBX" file (our vertexdata) and make a PrimitiveObject
            PrimitiveObject primitiveObject = new PrimitiveObject(
                GameConstants.Primitive_LitTexturedPyramid,
                ActorType.Decorator, //we could specify any time e.g. Pickup
                StatusType.Update | StatusType.Drawn,
                transform3D, effectParameters,
                vertexData);

            /*********** Controllers (optional) ***********/
            //we could add controllers to the archetype and then all clones would have cloned controllers
            //  drawnActor3D.ControllerList.Add(
            //new RotationController("rot controller1", ControllerType.RotationOverTime,
            //1, new Vector3(0, 1, 0)));

            //to do...add demos of controllers on archetypes
            //ensure that the Clone() method of PrimitiveObject will Clone() all controllers

            archetypeDictionary.Add(primitiveObject.ID, primitiveObject);
            #endregion Lit Textured Pyramid

            #region Unlit Textured Quad
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero,
                  Vector3.One, Vector3.UnitZ, Vector3.UnitY);

            effectParameters = new EffectParameters(
                effectDictionary[GameConstants.Effect_UnlitTextured],
                textureDictionary["34690"], Color.White, 1);

            vertexData = new VertexData<VertexPositionColorTexture>(
                VertexFactory.GetTextureQuadVertices(out primitiveType, out primitiveCount),
                primitiveType, primitiveCount);

            archetypeDictionary.Add(GameConstants.Primitive_UnlitTexturedQuad,
                new PrimitiveObject(GameConstants.Primitive_UnlitTexturedQuad,
                ActorType.Decorator,
                StatusType.Update | StatusType.Drawn,
                transform3D, effectParameters, vertexData));
            #endregion Unlit Textured Quad

            #region Unlit Origin Helper
            transform3D = new Transform3D(new Vector3(0, 20, 0),
                     Vector3.Zero, new Vector3(10, 10, 10),
                     Vector3.UnitZ, Vector3.UnitY);

            effectParameters = new EffectParameters(
                effectDictionary[GameConstants.Effect_UnlitWireframe],
                null, Color.White, 1);

            vertexData = new VertexData<VertexPositionColor>(VertexFactory.GetVerticesPositionColorOriginHelper(
                                    out primitiveType, out primitiveCount),
                                    primitiveType, primitiveCount);

            archetypeDictionary.Add(GameConstants.Primitive_WireframeOriginHelper,
                new PrimitiveObject(GameConstants.Primitive_WireframeOriginHelper,
                ActorType.Helper,
                StatusType.Update | StatusType.Drawn,
                transform3D, effectParameters, vertexData));

            #endregion Unlit Origin Helper

            #region Unlit Textured Octahedron
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero,
                  Vector3.One, Vector3.UnitZ, Vector3.UnitY);

            effectParameters = new EffectParameters(
                effectDictionary[GameConstants.Effect_UnlitTexturedPrimitive],
                textureDictionary["octahedron"], Color.White, 1);

            vertexData = new VertexData<VertexPositionColorTexture>(
                VertexFactory.GetTextureQuadVertices(out primitiveType, out primitiveCount),
                primitiveType, primitiveCount);

            archetypeDictionary.Add(GameConstants.Primitive_UnlitTexturedOctahedron,
                new PrimitiveObject(GameConstants.Primitive_UnlitTexturedOctahedron,
                ActorType.Decorator,
                StatusType.Update | StatusType.Drawn,
                transform3D, effectParameters, vertexData));
            #endregion Unlit Textured Octahedron

            #region Unlit Textured Cube
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero,
                  Vector3.One, Vector3.UnitZ, Vector3.UnitY);

            effectParameters = new EffectParameters(
                effectDictionary[GameConstants.Effect_UnlitTexturedPrimitive],
                textureDictionary["hero block"], Color.White, 1);

            vertices
                = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType,
                out primitiveCount);

            vertexData = new VertexData<VertexPositionNormalTexture>(vertices,
                primitiveType, primitiveCount);

            primitiveObject = new PrimitiveObject(
                GameConstants.Primitive_UnlitTexturedCube,
                ActorType.Decorator,
                StatusType.Drawn,
                transform3D, effectParameters,
                vertexData);

            archetypeDictionary.Add(primitiveObject.ID, primitiveObject);

            #endregion Unlit Textured Cube

            #region Unlit Textured Cube pickup
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero,
                  Vector3.One, Vector3.UnitZ, Vector3.UnitY);

            effectParameters = new EffectParameters(
                effectDictionary[GameConstants.Effect_UnlitTexturedPrimitive],
                textureDictionary["mystery block"], Color.White, 1);

            vertices
                = VertexFactory.GetVerticesPositionNormalTexturedCube(1, out primitiveType,
                out primitiveCount);

            vertexData = new VertexData<VertexPositionNormalTexture>(vertices,
                primitiveType, primitiveCount);

            primitiveObject = new PrimitiveObject(
                GameConstants.Primitive_UnlitTexturedCubePickup,
                ActorType.Decorator,
                StatusType.Update | StatusType.Drawn,
                transform3D, effectParameters,
                vertexData);

            archetypeDictionary.Add(primitiveObject.ID, primitiveObject);

            #endregion Unlit Textured Cube pickup
        }

        /// <summary>
        /// Demos how we can clone an archetype and manually add to the object manager.
        /// </summary>
        private void InitDecorators()
        {
            ////clone the archetypal pyramid
            //PrimitiveObject drawnActor3D
            //    = archetypeDictionary[GameConstants.Primitive_LitTexturedPyramid].Clone() as PrimitiveObject;

            ////change it a bit
            //drawnActor3D.ID = "pyramid1";
            //drawnActor3D.Transform3D.Scale = 10 * new Vector3(1, 1, 1);
            //drawnActor3D.Transform3D.RotationInDegrees = new Vector3(0, 0, 0);
            //drawnActor3D.Transform3D.Translation = new Vector3(0, 10, 0);
            //drawnActor3D.EffectParameters.Alpha = 0.5f;

            ////lets add a rotation controller so we can see all sides easily
            //drawnActor3D.ControllerList.Add(
            //    new RotationController("rot controller1", ControllerType.RotationOverTime,
            //    1, new Vector3(0, 1, 0)));

            ////drawnActor3D.ControllerList.Add(
            ////   new RotationController("rot controller2", ControllerType.RotationOverTime,
            ////   2, new Vector3(1, 0, 0)));

            ////finally add it into the objectmanager after SIX(!) steps
            //objectManager.Add(drawnActor3D);
        }

        
        private void InitHelpers()
        {
            //clone the archetype
            PrimitiveObject originHelper = archetypeDictionary[GameConstants.Primitive_WireframeOriginHelper].Clone() as PrimitiveObject;
            //add to the dictionary
            objectManager.Add(originHelper);
        }

        #endregion Initialization - Vertices, Archetypes, Helpers, Drawn Content(e.g. Skybox)

        #region Load & Unload Game Assets

        protected override void LoadContent()
        {
        }

        protected override void UnloadContent()
        {
            //housekeeping - unload content
            textureDictionary.Dispose();
            modelDictionary.Dispose();
            fontDictionary.Dispose();
            modelDictionary.Dispose();
            soundManager.Dispose();

            base.UnloadContent();
        }

        #endregion Load & Unload Game Assets

        #region Update & Draw

        protected override void Update(GameTime gameTime)
        {
            if (keyboardManager.IsFirstKeyPress(Keys.Escape))
            {
                Exit();
            }
            #region Demo
#if DEMO

            //#region Object Manager
            //if (keyboardManager.IsFirstKeyPress(Keys.R))
            //{
            //    EventDispatcher.Publish(new EventData(
            //    EventCategoryType.Object,
            //    EventActionType.OnApplyActionToFirstMatchActor,
            //    (actor) => actor.StatusType = StatusType.Drawn | StatusType.Update, //Action
            //    (actor) => actor.ActorType == ActorType.Decorator
            //    && actor.ID.Equals("pyramid1"), //Predicate
            //    null //parameters
            //    ));
            //}
            //#endregion Object Manager

            //#region Sound Demos
            //if (keyboardManager.IsFirstKeyPress(Keys.F1))
            //{
            //    // soundManager.Play2D("smokealarm");

            //    object[] parameters = { "smokealarm" };
            //    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
            //        EventActionType.OnPlay2D, parameters));
            //}
            //else if (keyboardManager.IsFirstKeyPress(Keys.F2))
            //{
            //    soundManager.Pause("smokealarm");

            //    object[] parameters = { "smokealarm" };
            //    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
            //        EventActionType.OnPause, parameters));
            //}
            //else if (keyboardManager.IsFirstKeyPress(Keys.F3))
            //{
            //    soundManager.Stop("smokealarm");

            //    object[] parameters = { "smokealarm" };
            //    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
            //        EventActionType.OnStop, parameters));
            //}
            //else if (keyboardManager.IsFirstKeyPress(Keys.F4))
            //{
            //    soundManager.SetMasterVolume(0);
            //}
            //else if (keyboardManager.IsFirstKeyPress(Keys.F5))
            //{
            //    soundManager.SetMasterVolume(0.5f);
            //}
            //else if (keyboardManager.IsFirstKeyPress(Keys.F6))
            //{
            //    AudioListener listener = new AudioListener();
            //    listener.Position = new Vector3(0, 5, 50);
            //    listener.Forward = -Vector3.UnitZ;
            //    listener.Up = Vector3.UnitY;

            //    AudioEmitter emitter = new AudioEmitter();
            //    emitter.DopplerScale = 1;
            //    emitter.Position = new Vector3(0, 5, 0);
            //    emitter.Forward = Vector3.UnitZ;
            //    emitter.Up = Vector3.UnitY;

            //    object[] parameters = { "smokealarm", listener, emitter };
            //    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
            //        EventActionType.OnPlay3D, parameters));
            //}
            //#endregion Sound Demos

            //#region Menu & UI Demos
            //if (keyboardManager.IsFirstKeyPress(Keys.F9))
            //{
            //    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPause, null));
            //}
            //else if (keyboardManager.IsFirstKeyPress(Keys.F10))
            //{
            //    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPlay, null));
            //}

            //if (keyboardManager.IsFirstKeyPress(Keys.Up))
            //{
            //    object[] parameters = { 1 }; //will increase the progress by 1 to its max of 10 (see InitUI)
            //    EventDispatcher.Publish(new EventData(EventCategoryType.UI, EventActionType.OnProgressDelta, parameters));
            //    gameStateManager.CollectCube();
            //}
            //else if (keyboardManager.IsFirstKeyPress(Keys.Down))
            //{
            //    object[] parameters = { -1 }; //will decrease the progress by 1 to its min of 0 (see InitUI)
            //    EventDispatcher.Publish(new EventData(EventCategoryType.UI, EventActionType.OnProgressDelta, parameters));
            //}

            //if (keyboardManager.IsFirstKeyPress(Keys.F5)) //game -> menu
            //{
            //    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPlay, null));
            //}
            //else if (keyboardManager.IsFirstKeyPress(Keys.F6)) //menu -> game
            //{
            //    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPause, null));
            //}
            //#endregion Menu & UI Demos

            //#region Camera
            //if (keyboardManager.IsFirstKeyPress(Keys.C))
            //{
            //    cameraManager.CycleActiveCamera();
            //    EventDispatcher.Publish(new EventData(EventCategoryType.Camera,
            //        EventActionType.OnCameraCycle, null));
            //}
            //#endregion Camera

#endif
            #endregion Demo

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            base.Draw(gameTime);
        }

        #endregion Update & Draw
    }
}