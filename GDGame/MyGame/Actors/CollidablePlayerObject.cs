﻿using GDGame.MyGame.Managers;
using GDLibrary.Actors;
using GDLibrary.Core.Managers.State;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.Parameters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary
{
    /// <summary>
    /// Moveable, collidable player using keyboard and checks for collisions
    /// </summary>
    public class CollidablePlayerObject : CollidablePrimitiveObject
    {
        #region Fields
        private float moveSpeed, rotationSpeed;
        private KeyboardManager keyboardManager;
        private MyGameStateManager gameStateManager;
        private Keys[] moveKeys;
        #endregion Fields

        public CollidablePlayerObject(string id, ActorType actorType, StatusType statusType, Transform3D transform,
            EffectParameters effectParameters, IVertexData vertexData,
            ICollisionPrimitive collisionPrimitive, ObjectManager objectManager,
            Keys[] moveKeys, float moveSpeed, float rotationSpeed, KeyboardManager keyboardManager, MyGameStateManager gameStateManager)
            : base(id, actorType, statusType, transform, effectParameters, vertexData, collisionPrimitive, objectManager)
        {
            this.moveKeys = moveKeys;
            this.moveSpeed = moveSpeed;
            this.rotationSpeed = rotationSpeed;

            //for movement
            this.keyboardManager = keyboardManager;
            this.gameStateManager = gameStateManager;
        }

        public override void Update(GameTime gameTime)
        {
            //read any input and store suggested increments
            HandleInput(gameTime);

            //have we collided with something?
            Collidee = CheckAllCollisions(gameTime);

            //how do we respond to this collidee e.g. pickup?
            HandleCollisionResponse(Collidee);

            if (Collidee == null)
            {
                ApplyInput(gameTime);
            }

            //reset translate and rotate and update primitive
            base.Update(gameTime);
        }

        protected override void HandleInput(GameTime gameTime)
        {

            if (keyboardManager.IsKeyDown(moveKeys[0])) //Forward
            {
                Transform3D.TranslateIncrement
                    = Transform3D.Look * gameTime.ElapsedGameTime.Milliseconds
                            * moveSpeed;
            }
            else if (keyboardManager.IsKeyDown(moveKeys[1])) //Backward
            {
                Transform3D.TranslateIncrement
                   = -Transform3D.Look * gameTime.ElapsedGameTime.Milliseconds
                           * moveSpeed;
            }

            if (keyboardManager.IsKeyDown(moveKeys[2])) //Left
            {
                Transform3D.RotateIncrement = gameTime.ElapsedGameTime.Milliseconds * rotationSpeed;
            }
            else if (keyboardManager.IsKeyDown(moveKeys[3])) //Right
            {
                Transform3D.RotateIncrement = -gameTime.ElapsedGameTime.Milliseconds * rotationSpeed;
            }

            //if (keyboardManager.IsKeyDown(moveKeys[4]) && isJumping == false) //Jump
            //{
                
            //}
        }

        /********************************************************************************************/

        //this is where you write the application specific CDCR response for your game
        protected override void HandleCollisionResponse(Actor collidee)
        {
            if (collidee is CollidableZoneObject)
            {
                CollidableZoneObject simpleZoneObject = collidee as CollidableZoneObject;

                //do something based on the zone type - see Main::InitializeCollidableZones() for ID
                if (simpleZoneObject.ID.Equals("camera trigger zone 1"))
                {
                    //publish an event e.g sound, health progress
                    object[] additionalParameters = { "boing" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay, additionalParameters));
                }

                //IMPORTANT - setting this to null means that the ApplyInput() method will get called and the player can move through the zone.
                Collidee = null;
            }
            else if (collidee is CollidablePrimitiveObject)
            {
                if (collidee.ActorType == ActorType.CollidablePickup)
                {
                    //if collidee is a pickup - send events to remove pickup, play a sound and increase progress bar
                    object[] parameters = { collidee };
                    EventDispatcher.Publish(new EventData(EventCategoryType.WinLose, EventActionType.OnPickup, parameters));

                    //remove the object
                    object[] additionalParameters = { collidee };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Object, EventActionType.OnRemoveActor, additionalParameters));

                    object[] additionalParameters2 = { "pickup" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, additionalParameters2));

                    object[] additionalParameters3 = { 1 }; //will increase the progress by 1 to its max of 5 (see InitUI)
                    EventDispatcher.Publish(new EventData(EventCategoryType.UI, EventActionType.OnProgressDelta, additionalParameters3));

                }
                else if (collidee.ActorType == ActorType.CollidableEnemy)
                {
                    //if collidee is an enemy - send events to play a sound, run lose game code and pause the game
                    object[] parameters = { "lose game" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPause, null));
                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnLose, null));
                }
            }
        }
    }
}