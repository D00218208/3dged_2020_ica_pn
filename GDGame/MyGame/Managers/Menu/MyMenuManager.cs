﻿using GDLibrary.Actors;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDGame.MyGame.Managers
{
    public class MyMenuManager : MenuManager
    {
        private MouseManager mouseManager;
        private KeyboardManager keyboardManager;

        public MyMenuManager(Game game, StatusType statusType, SpriteBatch spriteBatch,
            MouseManager mouseManager, KeyboardManager keyboardManager)
            : base(game, statusType, spriteBatch)
        {
            this.mouseManager = mouseManager;
            this.keyboardManager = keyboardManager;
        }

        public override void HandleEvent(EventData eventData)
        {
            if (eventData.EventCategoryType == EventCategoryType.Menu)
            {
                if (eventData.EventActionType == EventActionType.OnPause)
                    this.StatusType = StatusType.Drawn | StatusType.Update;
                else if (eventData.EventActionType == EventActionType.OnPlay)
                    this.StatusType = StatusType.Off;
                else if (eventData.EventActionType == EventActionType.OnLose)
                    //show the lose menu scene
                    this.SetScene("lose");
                else if (eventData.EventActionType == EventActionType.OnWin)
                    //show the win menu scene
                    this.SetScene("win");
            }
        }

        protected override void HandleInput(GameTime gameTime)
        {
            //bug fix - 7.12.20 - Exit button was hidden but we were still testing for mouse click
            if ((this.StatusType & StatusType.Update) != 0)
            {
                HandleMouse(gameTime);
            }

            HandleKeyboard(gameTime);
            //base.HandleInput(gameTime); //nothing happening in the base method
        }

        protected override void HandleMouse(GameTime gameTime)
        {
            foreach (DrawnActor2D actor in this.ActiveList)
            {
                if (actor is UIButtonObject)
                {
                    if (actor.Transform2D.Bounds.Contains(this.mouseManager.Bounds))
                    {
                        if (this.mouseManager.IsLeftButtonClickedOnce())
                        {
                            HandleClickedButton(gameTime, actor as UIButtonObject);
                        }
                    }
                }
            }
            base.HandleMouse(gameTime);
        }

        private void HandleClickedButton(GameTime gameTime, UIButtonObject uIButtonObject)
        {
            //send event to play sound for clicking any button
            object[] parameters = { "menu select" };
            EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

            //benefit of switch vs if...else is that code will jump to correct switch case directly
            switch (uIButtonObject.ID)
            {
                case "play":
                    this.SetScene("select level");
                    break;

                case "level 1":
                    //send an event to load level 1 and unpause
                    EventDispatcher.Publish(new EventData(EventCategoryType.Level, EventActionType.Level1, null));
                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPlay, null));
                    break;

                case "level 2":
                    //send an event to load level 2 and unpause
                    EventDispatcher.Publish(new EventData(EventCategoryType.Level, EventActionType.Level2, null));
                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPlay, null));
                    break;

                case "howtoplay":
                    //show the how to play menu scene
                    this.SetScene("howtoplay");
                    break;

                case "back":
                    //go back to the main menu from the how to play menu
                    this.SetScene("main");
                    break;

                case "exit":
                    this.Game.Exit();
                    break;

                case "menu":
                    //send an event to reset the object manager and cameras and show the main menu scene
                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnReset, null));
                    this.SetScene("main");
                    break;

                default:
                    break;
            }
        }

        protected override void HandleKeyboard(GameTime gameTime)
        {
            //if (this.keyboardManager.IsFirstKeyPress(Microsoft.Xna.Framework.Input.Keys.M))
            //{
            //    if (this.StatusType == StatusType.Off)
            //    {
            //        //show menu
            //        EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPause, null));
            //    }
            //    else
            //    {
            //        //show game
            //        EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPlay, null));
            //    }
            //}

            base.HandleKeyboard(gameTime);
        }
    }
}