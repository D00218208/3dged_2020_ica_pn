﻿using GDGame;
using GDLibrary.Actors;
using GDLibrary.Constants;
using GDLibrary.Containers;
using GDLibrary.Controllers;
using GDLibrary.Core.Controllers;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.Factories;
using GDLibrary.GameComponents;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.Parameters;
using GDLibrary.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace GDLibrary.Core.Managers.State
{
    /// <summary>
    /// Use this manager to listen for related events and perform actions in your game based on events received
    /// </summary>
    public class MyGameStateManager : PausableGameComponent, IEventHandler
    {
        private int collectedCubes;
        private MenuManager menuManager;
        private ObjectManager objectManager;
        private ContentDictionary<Texture2D> textureDictionary;
        private Dictionary<string, PrimitiveObject> archetypeDictionary;
        private Dictionary<string, Transform3DCurve> transform3DCurveDictionary;
        private CameraManager<Camera3D> cameraManager;
        private EventScheduler scheduler;
        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        private CollidablePlayerObject collidablePlayerObject;
        private Dictionary<string, BasicEffect> effectDictionary;
        Camera3D railCamera3D;
        Camera3D mainCamera3D;

        //get and set the amount of cube pickups collected by the player
        public int CollectedCubes
        {
            get
            {
                return collectedCubes;
            }
            set
            {
                collectedCubes = value;
            }
        }

        public MyGameStateManager(Game game, StatusType statusType, MenuManager menuManager, ObjectManager objectManager, ContentDictionary<Texture2D> textureDictionary,
            Dictionary<string, PrimitiveObject> archetypeDictionary, Dictionary<string, Transform3DCurve> transform3DCurveDictionary, CameraManager<Camera3D> cameraManager,
            KeyboardManager keyboardManager, MouseManager mouseManager, Dictionary<string, BasicEffect> effectDictionary) : base(game, statusType)
        {
            this.collectedCubes = 0;
            this.menuManager = menuManager;
            this.objectManager = objectManager;
            this.textureDictionary = textureDictionary;
            this.archetypeDictionary = archetypeDictionary;
            this.transform3DCurveDictionary = transform3DCurveDictionary;
            this.cameraManager = cameraManager;
            this.keyboardManager = keyboardManager;
            this.mouseManager = mouseManager;
            this.effectDictionary = effectDictionary;
        }

        //increment the amount of cubes collected by one
        public void CollectCube()
        {
            collectedCubes++;
        }


        public override void SubscribeToEvents()
        {
            EventDispatcher.Subscribe(EventCategoryType.WinLose, HandleEvent);
            EventDispatcher.Subscribe(EventCategoryType.Level, HandleEvent);
            EventDispatcher.Subscribe(EventCategoryType.Scheduler, HandleEvent);

            base.SubscribeToEvents();
        }

        public override void HandleEvent(EventData eventData)
        {
            if (eventData.EventActionType == EventActionType.OnPickup)
            {
                System.Diagnostics.Debug.WriteLine(collectedCubes);

                collectedCubes += 1;

                //if 6 cubes have been collected, send an event to run win game code
                //(5 cubes in each of the levels but a bug in level 1 causes the player to pick one up on spawn)
                if (this.collectedCubes == 6)
                {
                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPause, null));

                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnWin, null));
                }
            }
            else if (eventData.EventCategoryType == EventCategoryType.Level && eventData.EventActionType == EventActionType.Level1)
            {
                //load level 1 - block maze
                InitLevel(1);
            }
            else if (eventData.EventCategoryType == EventCategoryType.Level && eventData.EventActionType == EventActionType.Level2)
            {
                //load level 2 - dungeons and octahedrons
                InitLevel(2);
            }
            else if (eventData.EventCategoryType == EventCategoryType.Menu && eventData.EventActionType == EventActionType.OnPlay)
            {
                //send events to stop the menu music and play the game music if a level is selected
                object[] parameters = { "gamemusic" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                object[] parameters2 = { "menumusic" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters2));
            }
            else if (eventData.EventCategoryType == EventCategoryType.Menu && eventData.EventActionType == EventActionType.OnStop)
            {
                //send events to stop the game music and play the menu music if a level is finished
                object[] parameters = { "gamemusic" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters));

                object[] parameters2 = { "menumusic" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters2));
            }
            else if (eventData.EventCategoryType == EventCategoryType.Menu && eventData.EventActionType == EventActionType.OnPause)
            {
                //send events to stop the game music and play the menu music if the game is paused
                object[] parameters = { "gamemusic" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters));

                object[] parameters2 = { "menumusic" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters2));
            }
            else if (eventData.EventCategoryType == EventCategoryType.Menu && eventData.EventActionType == EventActionType.OnReset)
            {
                //clear the object manager and camera manager on level completion
                objectManager.Clear();

                object[] parameters = { -collectedCubes }; //reset progress bar
                EventDispatcher.Publish(new EventData(EventCategoryType.UI, EventActionType.OnProgressDelta, parameters));

                collectedCubes = 0;

                cameraManager.list.Clear();

                //workaround to avoid camera index out of range bug
                Transform3D transform3D = null;
                Camera3D camera3D = null;
                Viewport viewPort = new Viewport(0, 0, 1024, 768);

                transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.Zero);

                camera3D = new Camera3D(GameConstants.Camera_NonCollidableCurveMainArena,
                  ActorType.Camera3D, StatusType.Update, transform3D,
                            ProjectionParameters.StandardDeepSixteenTen, viewPort);

                camera3D.ControllerList.Add(
                    new Curve3DController(GameConstants.Controllers_NonCollidableCurveMainArena,
                    ControllerType.Curve,
                            transform3DCurveDictionary["enemy 1 route"]));

                cameraManager.Add(camera3D);
            }
            else if (eventData.EventCategoryType == EventCategoryType.Scheduler && eventData.EventActionType == EventActionType.OnCameraCycle)
            {
                //change active camera to main camera once intro camera has finished
                cameraManager.ActiveCameraIndex = 2;
            }
            base.HandleEvent(eventData);
        }

        private void InitLevel(int levelNum)//, List<string> levelNames)
        {   
            /************ Level-loader (can be collidable or non-collidable) ************/

            LevelLoader<PrimitiveObject> levelLoader = new LevelLoader<PrimitiveObject>(
                this.archetypeDictionary, this.textureDictionary, this.objectManager);
            List<DrawnActor3D> actorList = null;

            //generate random layout for level 1 maze
            System.Random random = new System.Random();
            int layout = random.Next(1, 4);

            switch (levelNum)
            {
                case 1:
                    InitPlayer();
                    InitCameras3D();

                    switch (layout)
                    {
                        case 1:
                            //add level1_1 contents 
                            actorList = levelLoader.Load(
                                this.textureDictionary["level1_1"],
                                            20,     //number of in-world x-units represented by 1 pixel in image
                                            20,     //number of in-world z-units represented by 1 pixel in image
                                            11,     //y-axis height offset
                                            new Vector3(-50, 0, -150) //offset to move all new objects by
                                            );
                            this.objectManager.Add(actorList);

                            //clear the list otherwise when we add level1_2 we would re-add level1_1 objects to object manager
                            actorList.Clear();
                            break;
                        case 2:
                            actorList = levelLoader.Load(
                                this.textureDictionary["level1_2"],
                                            20,     //number of in-world x-units represented by 1 pixel in image
                                            20,     //number of in-world z-units represented by 1 pixel in image
                                            11,     //y-axis height offset
                                            new Vector3(-50, 0, -150) //offset to move all new objects by
                                            );
                            this.objectManager.Add(actorList);

                            actorList.Clear();
                            break;
                        case 3:
                            actorList = levelLoader.Load(
                                this.textureDictionary["level1_3"],
                                            20,     //number of in-world x-units represented by 1 pixel in image
                                            20,     //number of in-world z-units represented by 1 pixel in image
                                            11,     //y-axis height offset
                                            new Vector3(-50, 0, -150) //offset to move all new objects by
                                            );
                            this.objectManager.Add(actorList);

                            actorList.Clear();
                            break;
                    }

                    InitGround(2000);
                    InitSkybox(2000);

                    //make a scheduler to change the camera to the main one after the intro camera finishes
                    scheduler = new EventScheduler("change camera");

                    object[] additionalParameters = { null };
                    scheduler.Add(new EventData(EventCategoryType.Scheduler, EventActionType.OnCameraCycle, additionalParameters), 7000);
                    scheduler.Start();
                    break;
                case 2:
                    switch (layout)
                    {
                        case 1:
                            //add level1_1 contents 
                            actorList = levelLoader.Load(
                                this.textureDictionary["level2_1"],
                                            20,     //number of in-world x-units represented by 1 pixel in image
                                            20,     //number of in-world z-units represented by 1 pixel in image
                                            11,     //y-axis height offset
                                            new Vector3(-50, 0, -150) //offset to move all new objects by
                                            );
                            this.objectManager.Add(actorList);

                            //clear the list otherwise when we add level1_2 we would re-add level1_1 objects to object manager
                            actorList.Clear();
                            break;
                        case 2:
                            actorList = levelLoader.Load(
                                this.textureDictionary["level2_2"],
                                            20,     //number of in-world x-units represented by 1 pixel in image
                                            20,     //number of in-world z-units represented by 1 pixel in image
                                            11,     //y-axis height offset
                                            new Vector3(-50, 0, -150) //offset to move all new objects by
                                            );
                            this.objectManager.Add(actorList);

                            actorList.Clear();
                            break;
                        case 3:
                            actorList = levelLoader.Load(
                                this.textureDictionary["level2_3"],
                                            20,     //number of in-world x-units represented by 1 pixel in image
                                            20,     //number of in-world z-units represented by 1 pixel in image
                                            11,     //y-axis height offset
                                            new Vector3(-50, 0, -150) //offset to move all new objects by
                                            );
                            this.objectManager.Add(actorList);

                            actorList.Clear();
                            break;
                    }
                    InitEnemies();

                    //workaround in level 2 to add pickup that level 1 automatically adds on start due to a bug
                    object[] additionalParameters2 = { "pickup" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, additionalParameters2));

                    object[] parameters = { null };
                    EventDispatcher.Publish(new EventData(EventCategoryType.WinLose, EventActionType.OnPickup, parameters));

                    object[] additionalParameters3 = { 1 }; //will increase the progress by 1 to its max of 5 (see InitUI)
                    EventDispatcher.Publish(new EventData(EventCategoryType.UI, EventActionType.OnProgressDelta, additionalParameters3));

                    InitPlayer();
                    InitCameras3D();
                    InitGround(2000);
                    InitSkybox(2000);

                    //make a scheduler to change the camera to the main one after the intro camera finishes
                    scheduler = new EventScheduler("change camera");

                    object[] additionalParameters4 = { null };
                    scheduler.Add(new EventData(EventCategoryType.Scheduler, EventActionType.OnCameraCycle, additionalParameters4), 7200);
                    scheduler.Start();
                    break;
            }
        }

        private void InitCameras3D()
        {
            Transform3D transform3D = null;
            Viewport viewPort = new Viewport(0, 0, 1024, 768);

            //intro camera
            #region Noncollidable Camera - Curve3D

            //notice that it doesnt matter what translation, look, and up are since curve will set these
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.Zero);

            railCamera3D = new Camera3D(GameConstants.Camera_NonCollidableCurveMainArena,
              ActorType.Camera3D, StatusType.Update, transform3D,
                        ProjectionParameters.StandardDeepSixteenTen, viewPort);

            railCamera3D.ControllerList.Add(
                new Curve3DController(GameConstants.Controllers_NonCollidableCurveMainArena,
                ControllerType.Curve,
                        transform3DCurveDictionary["camera curve"])); //use the curve dictionary to retrieve a transform3DCurve by id

            cameraManager.Add(railCamera3D);

            #endregion Noncollidable Camera - Curve3D

            //main camera
            #region Noncollidable Camera - Third Person

            transform3D = new Transform3D(Vector3.Zero,
                Vector3.Zero,
                Vector3.UnitY);

            mainCamera3D = new Camera3D(GameConstants.Camera_CollidableThirdPerson,
                ActorType.Camera3D, StatusType.Update, transform3D,
                ProjectionParameters.StandardDeepSixteenTen, viewPort);

            //attach a controller
            mainCamera3D.ControllerList.Add(new ThirdPersonController(
                GameConstants.Camera_CollidableThirdPerson,
                ControllerType.ThirdPerson, this, collidablePlayerObject,
                165,
                18, LerpSpeed.Slow, mouseManager));

            cameraManager.Add(mainCamera3D);
            #endregion Noncollidable Camera - Third Person

            cameraManager.ActiveCameraIndex = 1; //0, 1, 2, 3, 4
        }

        /// <summary>
        /// Creates a collidable player object for the player to control
        /// </summary>
        private void InitPlayer()
        {
            Transform3D transform3D = null;
            EffectParameters effectParameters = null;
            IVertexData vertexData = null;
            ICollisionPrimitive collisionPrimitive = null;
            PrimitiveType primitiveType;
            int primitiveCount;

            transform3D = new Transform3D(new Vector3(250, 5, 410), Vector3.Zero, new Vector3(6, 6, 6),
                -Vector3.UnitZ, Vector3.UnitY);

            effectParameters = new EffectParameters(
                effectDictionary[GameConstants.Effect_UnlitTexturedPrimitive],
                textureDictionary["hero block"], Color.White, 1);

            vertexData = new VertexData<VertexPositionNormalTexture>(
                VertexFactory.GetVerticesPositionNormalTexturedCube(1,
                out primitiveType, out primitiveCount),
                primitiveType, primitiveCount);

            collisionPrimitive = new BoxCollisionPrimitive(transform3D);

            collidablePlayerObject
                = new CollidablePlayerObject("collidable player1",
                    ActorType.CollidablePlayer,
                    StatusType.Drawn | StatusType.Update,
                    transform3D,
                    effectParameters,
                    vertexData,
                    collisionPrimitive,
                    objectManager,
                    GameConstants.KeysOne,
                    GameConstants.playerMoveSpeed,
                    GameConstants.playerRotateSpeed,
                    keyboardManager, this);

            objectManager.Add(collidablePlayerObject);
        }

        /// <summary>
        /// Creates two collidable enemy objects based on the octahedron archetype and adds controllers to them
        /// </summary>
        private void InitEnemies()
        {
            #region Enemy 1
            PrimitiveType primitiveType;
            int primitiveCount;

            Transform3D transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, new Vector3(8, 8, 8), Vector3.UnitZ, Vector3.UnitY);

            PrimitiveObject archetype
                    = archetypeDictionary["unlit textured octahedron"] as PrimitiveObject;

            PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

            drawnActor3D.ID = "enemy";
            drawnActor3D.Transform3D.Scale = 20 * new Vector3(0.7f, 0.7f, 0.7f);
            drawnActor3D.EffectParameters.DiffuseColor = Color.White;
            drawnActor3D.EffectParameters.Alpha = 1;
            drawnActor3D.Transform3D.Translation = new Vector3(250, 6, 390);
            drawnActor3D.Transform3D.RotationInDegrees = new Vector3(90, 0, 0);

            IVertexData vertexData = new VertexData<VertexPositionNormalTexture>(
            VertexFactory.GetVerticesPositionTexturedOctahedron(1,
                              out primitiveType, out primitiveCount),
                              primitiveType, primitiveCount);

            ICollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(drawnActor3D.Transform3D);

            CollidablePrimitiveObject collidablePrimitiveObject = new CollidablePrimitiveObject(
            GameConstants.Primitive_UnlitTexturedOctahedron,
            ActorType.CollidableEnemy, 
            StatusType.Drawn | StatusType.Update,
            transform3D,
            drawnActor3D.EffectParameters,
            vertexData,
            collisionPrimitive, objectManager);

            collidablePrimitiveObject.Transform3D.Translation = new Vector3(250, 6, 390);

            //add rotation controller for rotation while moving
            collidablePrimitiveObject.ControllerList.Add(
                new RotationController("rot controller1", ControllerType.RotationOverTime,
                1.5f, new Vector3(0, 1, 0)));

            //add curve 3D controller for enemy to move along a predefined curve
            collidablePrimitiveObject.ControllerList.Add(
                new Curve3DController("enemy 1 path",
                ControllerType.Curve,
                        transform3DCurveDictionary["enemy 1 route"]));

            this.objectManager.Add(collidablePrimitiveObject);
            #endregion Enemy 1

            #region Enemy 2
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, new Vector3(8, 8, 8), Vector3.UnitZ, Vector3.UnitY);

            archetype
                    = archetypeDictionary["unlit textured octahedron"] as PrimitiveObject;

            drawnActor3D = archetype.Clone() as PrimitiveObject;

            drawnActor3D.ID = "enemy";
            drawnActor3D.Transform3D.Scale = 20 * new Vector3(0.7f, 0.7f, 0.7f);
            drawnActor3D.EffectParameters.DiffuseColor = Color.White;
            drawnActor3D.EffectParameters.Alpha = 1;
            drawnActor3D.Transform3D.Translation = new Vector3(250, 6, 390);
            drawnActor3D.Transform3D.RotationInDegrees = new Vector3(90, 0, 0);

            vertexData = new VertexData<VertexPositionNormalTexture>(
            VertexFactory.GetVerticesPositionTexturedOctahedron(1,
                              out primitiveType, out primitiveCount),
                              primitiveType, primitiveCount);

            collisionPrimitive = new BoxCollisionPrimitive(drawnActor3D.Transform3D);

            collidablePrimitiveObject = new CollidablePrimitiveObject(
            GameConstants.Primitive_UnlitTexturedOctahedron,
            ActorType.CollidableEnemy,  //this is important as it will determine how we filter collisions in our collidable player CDCR code
            StatusType.Drawn | StatusType.Update,
            transform3D,
            drawnActor3D.EffectParameters,
            vertexData,
            collisionPrimitive, objectManager);

            collidablePrimitiveObject.Transform3D.Translation = new Vector3(250, 6, 390);

            collidablePrimitiveObject.ControllerList.Add(
                new RotationController("rot controller1", ControllerType.RotationOverTime,
                1.5f, new Vector3(0, 1, 0)));

            collidablePrimitiveObject.ControllerList.Add(
                new Curve3DController("enemy 2 path",
                ControllerType.Curve,
                        transform3DCurveDictionary["enemy 2 route"]));

            this.objectManager.Add(collidablePrimitiveObject);
            #endregion Enemy 2
        }

        private void InitGround(float worldScale)
        {
            PrimitiveObject drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            drawnActor3D.ActorType = ActorType.Ground;
            drawnActor3D.EffectParameters.Texture = textureDictionary["34636"];
            drawnActor3D.Transform3D.RotationInDegrees = new Vector3(-90, 0, 0);
            drawnActor3D.Transform3D.Scale = worldScale * Vector3.One;
            objectManager.Add(drawnActor3D);
        }

        private void InitSkybox(float worldScale)
        {
            PrimitiveObject drawnActor3D = null;

            //back
            drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            drawnActor3D.ActorType = ActorType.Sky;

            //  primitiveObject.StatusType = StatusType.Off; //Experiment of the effect of StatusType
            drawnActor3D.ID = "sky front";
            drawnActor3D.EffectParameters.Texture = textureDictionary["front"]; ;
            drawnActor3D.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            drawnActor3D.Transform3D.Translation = new Vector3(0, 0, -worldScale / 2.0f);
            objectManager.Add(drawnActor3D);

            //left
            drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            drawnActor3D.ActorType = ActorType.Sky;
            drawnActor3D.ID = "left back";
            drawnActor3D.EffectParameters.Texture = textureDictionary["left"]; ;
            drawnActor3D.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            drawnActor3D.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            drawnActor3D.Transform3D.Translation = new Vector3(-worldScale / 2.0f, 0, 0);
            objectManager.Add(drawnActor3D);

            //right
            drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            drawnActor3D.ActorType = ActorType.Sky;
            drawnActor3D.ID = "sky right";
            drawnActor3D.EffectParameters.Texture = textureDictionary["right"];
            drawnActor3D.Transform3D.Scale = new Vector3(worldScale, worldScale, 20);
            drawnActor3D.Transform3D.RotationInDegrees = new Vector3(0, -90, 0);
            drawnActor3D.Transform3D.Translation = new Vector3(worldScale / 2.0f, 0, 0);
            objectManager.Add(drawnActor3D);

            //top
            drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            drawnActor3D.ActorType = ActorType.Sky;
            drawnActor3D.ID = "sky top";
            drawnActor3D.EffectParameters.Texture = textureDictionary["top"];
            drawnActor3D.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            drawnActor3D.Transform3D.RotationInDegrees = new Vector3(90, 0, 0);
            drawnActor3D.Transform3D.Translation = new Vector3(0, worldScale / 2.0f, 0);
            objectManager.Add(drawnActor3D);

            //front
            drawnActor3D = archetypeDictionary[GameConstants.Primitive_UnlitTexturedQuad].Clone() as PrimitiveObject;
            drawnActor3D.ActorType = ActorType.Sky;
            drawnActor3D.ID = "sky back";
            drawnActor3D.EffectParameters.Texture = textureDictionary["back"];
            drawnActor3D.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            drawnActor3D.Transform3D.RotationInDegrees = new Vector3(0, 180, 0);
            drawnActor3D.Transform3D.Translation = new Vector3(0, 0, worldScale / 2.0f);
            objectManager.Add(drawnActor3D);
        }

        protected override void ApplyUpdate(GameTime gameTime)
        {
            base.ApplyUpdate(gameTime);
        }
    }
}