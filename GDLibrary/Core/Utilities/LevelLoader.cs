﻿using GDGame;
using GDLibrary.Actors;
using GDLibrary.Containers;
using GDLibrary.Controllers;
using GDLibrary.Enums;
using GDLibrary.Factories;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.Parameters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace GDLibrary.Utilities
{
    /// <summary>
    /// Use the level loader to instanciate 3D drawn actors within your level from a PNG file.
    ///
    /// Usage:
    ///    LevelLoader levelLoader = new LevelLoader(this.objectArchetypeDictionary,
    ///    this.textureDictionary);
    ///     List<DrawnActor3D> actorList = levelLoader.Load(this.textureDictionary[fileName],
    ///           scaleX, scaleZ, height, offset);
    ///     this.object3DManager.Add(actorList);
    ///
    /// </summary>
    public class LevelLoader<T> where T : DrawnActor3D
    {
        private static readonly Color ColorLevelLoaderIgnore = Color.White;

        private Dictionary<string, T> archetypeDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private ObjectManager objectManager;

        public LevelLoader(Dictionary<string, T> archetypeDictionary,
            ContentDictionary<Texture2D> textureDictionary, ObjectManager objectManager)
        {
            this.archetypeDictionary = archetypeDictionary;
            this.textureDictionary = textureDictionary;
            this.objectManager = objectManager;
        }

        public List<DrawnActor3D> Load(Texture2D texture,
            float scaleX, float scaleZ, float height, Vector3 offset)
        {
            List<DrawnActor3D> list = new List<DrawnActor3D>();
            Color[] colorData = new Color[texture.Height * texture.Width];
            texture.GetData<Color>(colorData);

            Color color;
            Vector3 translation;
            DrawnActor3D actor;

            for (int y = 0; y < texture.Height; y++)
            {
                for (int x = 0; x < texture.Width; x++)
                {
                    color = colorData[x + y * texture.Width];

                    if (!color.Equals(ColorLevelLoaderIgnore))
                    {
                        //scale allows us to increase the separation between objects in the XZ plane
                        translation = new Vector3(x * scaleX, height, y * scaleZ);

                        //the offset allows us to shift the whole set of objects in X, Y, and Z
                        translation += offset;

                        actor = getObjectFromColor(color, translation);

                        if (actor != null)
                        {
                            list.Add(actor);
                        }
                    }
                } //end for x
            } //end for y
            return list;
        }

        private Random rand = new Random();

        private int count = 1;

        private DrawnActor3D getObjectFromColor(Color color, Vector3 translation)
        {
            //if the pixel is blue then draw a wall
            if (color.Equals(new Color(0, 0, 255)))
            {
                PrimitiveType primitiveType;
                int primitiveCount;

                PrimitiveObject archetype
                        = archetypeDictionary["unlit textured cube"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                drawnActor3D.ID = "wall " + count++;
                drawnActor3D.Transform3D.Scale = 10 * new Vector3(2, 2, 2);
                drawnActor3D.EffectParameters.DiffuseColor = Color.Green;
                drawnActor3D.EffectParameters.Alpha = 0.85f;
                drawnActor3D.Transform3D.Translation = translation;
                drawnActor3D.Transform3D.RotationInDegrees = new Vector3(90, 0, 0);

                IVertexData vertexData = new VertexData<VertexPositionNormalTexture>(
                VertexFactory.GetVerticesPositionNormalTexturedCube(1,
                                  out primitiveType, out primitiveCount),
                                  primitiveType, primitiveCount);

                ICollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(drawnActor3D.Transform3D);

                CollidablePrimitiveObject collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_UnlitTexturedCube,
                ActorType.CollidableEnemy,
                StatusType.Drawn | StatusType.Update,
                drawnActor3D.Transform3D,
                drawnActor3D.EffectParameters,
                vertexData,
                collisionPrimitive, objectManager);

                collidablePrimitiveObject.Transform3D.Translation = translation;

                return collidablePrimitiveObject;
            }
            //if the pixel is red then draw a pickup
            else if (color.Equals(new Color(255, 0, 0)))
            {
                PrimitiveType primitiveType;
                int primitiveCount;

                Transform3D transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, new Vector3(5, 5, 5), Vector3.UnitZ, Vector3.UnitY);

                PrimitiveObject archetype
                        = archetypeDictionary["unlit textured cube pickup"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                drawnActor3D.ID = "pickup " + count++;
                drawnActor3D.Transform3D.Scale = 10 * new Vector3(0.7f, 0.7f, 0.7f);
                drawnActor3D.EffectParameters.DiffuseColor = Color.White;
                drawnActor3D.EffectParameters.Alpha = 1;
                drawnActor3D.Transform3D.Translation = translation;
                drawnActor3D.Transform3D.RotationInDegrees = new Vector3(90, 0, 0);

                IVertexData vertexData = new VertexData<VertexPositionNormalTexture>(
                VertexFactory.GetVerticesPositionNormalTexturedCube(1,
                                  out primitiveType, out primitiveCount),
                                  primitiveType, primitiveCount);

                ICollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(drawnActor3D.Transform3D);

                CollidablePrimitiveObject collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_UnlitTexturedCube,
                ActorType.CollidablePickup,  //this is important as it will determine how we filter collisions in our collidable player CDCR code
                StatusType.Drawn | StatusType.Update,
                transform3D,
                drawnActor3D.EffectParameters,
                vertexData,
                collisionPrimitive, objectManager);

                collidablePrimitiveObject.Transform3D.Translation = translation;

                collidablePrimitiveObject.ControllerList.Add(
                    new RotationController("rot controller1", ControllerType.RotationOverTime,
                    1.5f, new Vector3(0, 1, 0)));

                return collidablePrimitiveObject;
            }

            //if the pixel is green then draw an outer wall
            else if (color.Equals(new Color(0, 255, 0)))
            {
                PrimitiveType primitiveType;
                int primitiveCount;

                PrimitiveObject archetype
                        = archetypeDictionary["lit textured pyramid"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                drawnActor3D.ID = "outer wall " + count++;
                drawnActor3D.Transform3D.Scale = 10 * new Vector3(2, 4, 2);
                drawnActor3D.EffectParameters.DiffuseColor = Color.Red;
                drawnActor3D.EffectParameters.Alpha = 0.9f;
                drawnActor3D.Transform3D.Translation = translation;
                drawnActor3D.Transform3D.RotationInDegrees = new Vector3(0, 0, 0);

                IVertexData vertexData = new VertexData<VertexPositionNormalTexture>(
                VertexFactory.GetVerticesPositionNormalTexturedPyramid(
                                  out primitiveType, out primitiveCount),
                                  primitiveType, primitiveCount);

                ICollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(drawnActor3D.Transform3D);

                CollidablePrimitiveObject collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_LitTexturedPyramid,
                ActorType.CollidableEnemy,
                StatusType.Drawn | StatusType.Update,
                drawnActor3D.Transform3D,
                drawnActor3D.EffectParameters,
                vertexData,
                collisionPrimitive, objectManager);

                collidablePrimitiveObject.Transform3D.Translation = translation + new Vector3 (0, -10, 0);

                return collidablePrimitiveObject;
            }
            //if the pixel is yellow then draw a brick wall
            else if (color.Equals(new Color(255, 255, 0)))
            {
                PrimitiveType primitiveType;
                int primitiveCount;

                PrimitiveObject archetype
                        = archetypeDictionary["unlit textured cube"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                drawnActor3D.ID = "brick wall " + count++;
                drawnActor3D.Transform3D.Scale = 10 * new Vector3(2, 4, 2);
                drawnActor3D.EffectParameters.Alpha = 1;
                drawnActor3D.EffectParameters.Texture = textureDictionary["brick"];
                drawnActor3D.Transform3D.Translation = translation;
                drawnActor3D.Transform3D.RotationInDegrees = new Vector3(0, 0, 0);

                IVertexData vertexData = new VertexData<VertexPositionNormalTexture>(
                VertexFactory.GetVerticesPositionNormalTexturedCube(1,
                                  out primitiveType, out primitiveCount),
                                  primitiveType, primitiveCount);

                ICollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(drawnActor3D.Transform3D);

                CollidablePrimitiveObject collidablePrimitiveObject = new CollidablePrimitiveObject(
                GameConstants.Primitive_UnlitTexturedCube,
                ActorType.CollidableEnemy,
                StatusType.Drawn | StatusType.Update,
                drawnActor3D.Transform3D,
                drawnActor3D.EffectParameters,
                vertexData,
                collisionPrimitive, objectManager);

                collidablePrimitiveObject.Transform3D.Translation = translation + new Vector3(0, -10, 0);

                return collidablePrimitiveObject;
            }

            return null;
        }
    }
}